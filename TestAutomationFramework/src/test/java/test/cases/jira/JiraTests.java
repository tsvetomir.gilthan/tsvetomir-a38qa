package test.cases.jira;

import com.telerikacademy.testframework.Utils;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pages.jira.CreateStory;
import pages.jira.LoginPage;
import pages.jira.ProjectsPage;

public class JiraTests extends BaseTest {

    @Test
    public void login(){
            LoginPage loginPage = new LoginPage(actions.getDriver());
            loginPage.loginUser("user");




 }
    @Test
    public void navigateToAllProjects(){
        ProjectsPage projectsPage = new ProjectsPage(actions.getDriver());
        projectsPage.navigateToProjectsUI();
        projectsPage.selectProject();

        CreateStory createStory = new CreateStory(actions.getDriver());
        createStory.actions.assertElementPresent("jira.createStoryPage.createGlobalItemButton");
        createStory.createStory_Bug();

        CreateStory issueTypeMenu = new CreateStory(actions.getDriver());
        issueTypeMenu.actions.assertElementPresent("jira.createStoryPage.SelectIssueType");
        issueTypeMenu.clickOnIssueTypeMenu();
        issueTypeMenu.inputSummary("Summary");

    }




   /* @Test
    public void navigateToProject(){
    ProjectsPage projectPage = new ProjectsPage(actions.getDriver());
    projectPage.selectProject("jira.projectsPage.selectProject");


    }*/
   /*@Test
   public void createProject(){
       ProjectsPage projectsPage = new ProjectsPage(actions.getDriver());
       projectsPage.clickOnCreateProjectButton("test");
       projectsPage.clickOnSelectTemplateButton();
       projectsPage.clickOnUseTemplateButton();

   }*/





/*@Test
public void enterProjectName_and_createProject(){                                               //can be fixed
        ProjectsPage projectName = new ProjectsPage(actions.getDriver());
        projectName.submitAndCreateProject("jira.createProjectPage.searchText");

        ProjectsPage createdProject = new ProjectsPage(actions.getDriver());
        createdProject.assertProjectIsPresent(Utils.getUIMappingByKey("jira.createProjectPage.searchText"));

}*/


}

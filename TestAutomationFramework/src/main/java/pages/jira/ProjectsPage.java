package pages.jira;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ProjectsPage extends BaseJiraPage {

    public ProjectsPage(WebDriver driver) {
        super(driver, "jira.projectsUrl");
    }
    public void navigateToProjectsUI() {

        actions.waitForElementVisibleUntilTimeout(
                "jira.startPage.openPersonalProjectsButton", 5);
        actions.clickElement("jira.startPage.openPersonalProjectsButton");


    }

    public void selectProject(){
        actions.waitForElementVisibleUntilTimeout("jira.projectsPage.selectProject",10);
        actions.clickElement("jira.projectsPage.selectProject");

    }











                //ToDo CreateProject
    public void clickOnCreateProjectButton(String myProjects){
        actions.waitForElementVisibleUntilTimeout(
                "jira.accountProjectsPage.createProjectButton",5,myProjects);
        actions.clickElement("jira.accountProjectsPage.createProjectButton",myProjects);


    }

    public void clickOnSelectTemplateButton(){

        actions.waitForElementVisibleUntilTimeout(
                "jira.createProjectPage.selectProjectTemplateScrumButton",5);
        actions.clickElement("jira.createProjectPage.selectProjectTemplateScrumButton");


    }
    public void clickOnUseTemplateButton(){
        actions.waitForElementVisibleUntilTimeout("jira.createProjectPage.useTemplateButton",5);
        actions.clickElement("jira.createProjectPage.useTemplateButton");

        actions.waitForElementVisibleUntilTimeout("jira.createProjectPage.selectCompanyManagedProject",5);
        actions.clickElement("jira.createProjectPage.selectCompanyManagedProject");
    }


    /* public void enterProjectName(String term){                                   //ToDo can be fixed
        actions.typeValueInField(term,"jira.createProjectPage.searchInput");
    }

    public void submitAndCreateProject(String term){
        actions.waitForElementVisibleUntilTimeout("jira.createProjectPage.createProjectButton",3);
        actions.clickElement("jira.createProjectPage.createProjectButton");
        enterProjectName(term);


    }*/
}

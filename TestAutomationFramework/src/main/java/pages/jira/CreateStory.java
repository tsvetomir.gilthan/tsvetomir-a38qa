package pages.jira;

import com.telerikacademy.testframework.Utils;
import org.openqa.selenium.WebDriver;

public class CreateStory  extends BaseJiraPage{
    public CreateStory(WebDriver driver){super(driver,"jira.SeleniumProjectPage");}

   /* public void navigateToProject(){
        actions.waitForElementVisibleUntilTimeout("jira.SeleniumProjectPage",5);
        actions.clickElement("jira.SeleniumProjectPage.createProjectButton");


    }
*/
    public void createStory_Bug(){
        actions.waitForElementVisibleUntilTimeout("jira.createStoryPage.createGlobalItemButton",6);
        actions.clickElement("jira.createStoryPage.createGlobalItemButton");

    }

    public void clickOnIssueTypeMenu(){
        actions.waitForElementVisibleUntilTimeout("jira.createStoryPage.SelectIssueType",5);
        actions.clickElement("jira.createStoryPage.SelectIssueType");


    }

    public void inputSummary(String txt){
        String summary = Utils.getConfigPropertyByKey("jira.summary" + txt + ".summary");
        actions.waitForElementVisibleUntilTimeout("jira.createStoryPage.InputSummary",5);
        actions.clickElement("jira.createStoryPage.InputSummary");
        actions.typeValueInField(summary,"jira.createStoryPage.Summary");

    }



}

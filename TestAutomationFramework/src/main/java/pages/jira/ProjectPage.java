package pages.jira;

import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;


public class ProjectPage  extends BasePage {
    public ProjectPage(WebDriver driver){super(driver,"jira.projectPage");}



    public void assertProjectIsPresent(String projectTitle){
        actions.assertElementPresent("jira.projectPage");
        actions.assertElementAttribute("jira.projectPage","innerText",projectTitle);

    }
}

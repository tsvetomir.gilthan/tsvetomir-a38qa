package SeleniumTest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.Duration;


public class Bing {


    private String baseUrl;




    @Test
    public void findXpath()  {
        System.setProperty("webdriver.chrome.driver",                    // driver path
                "C:\\Users\\Dan4o\\Downloads\\geckodriver-v0.31.0-win64\\chromedriver103.exe");

        WebDriver driver = new ChromeDriver(); //create instance of a driver
        baseUrl = "https://bing.com";

        driver.get(baseUrl);
        driver.manage().window().maximize();


        WebElement wait = new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.elementToBeClickable(By.linkText("Accept")));

        WebElement accept_btn = driver.findElement(By.linkText("Accept")); //By.linkText
        accept_btn.click();


        WebElement searchBar =  driver.findElement(By.id("sb_form_q")); //By.id
        searchBar.click();
        searchBar.sendKeys("Telerik Academy Alpha");
        searchBar.submit();

        WebElement firstResult = new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.elementToBeClickable(By.linkText(
                        "IT Career Start in 6 Months - Telerik Academy Alpha")));
      //  firstResult.click();


        Assert.assertTrue(
                firstResult.isDisplayed(), "IT Career Start in 6 Months - Telerik Academy Alpha is displayed");

        System.out.println(firstResult);

        driver.quit();
    }


}


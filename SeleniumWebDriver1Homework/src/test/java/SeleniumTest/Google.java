package SeleniumTest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.Duration;


public class Google {

    private WebDriver driver;
    private String baseUrl;



    @Test
    public void findXpath()  {
        System.setProperty("webdriver.chrome.driver",                    // driver path
                "C:\\Users\\Dan4o\\Downloads\\geckodriver-v0.31.0-win64\\chromedriver103.exe");

        WebDriver driver = new ChromeDriver(); //create instance of a driver
        baseUrl = "https://google.com";

        driver.get(baseUrl);
        driver.manage().window().maximize();




        driver.findElement(By.cssSelector("button[id='L2AGLb'] div[role='none']")).click(); //By.cssSelector    path of Search box

       WebElement searchBar =  driver.findElement(By.xpath("(//input[@title='Търсене'])[1]")); //By.xpath
        searchBar.click();
        searchBar.sendKeys("Telerik Academy Alpha");
        searchBar.submit();
        WebElement firstResult = new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h3[contains(text(),'IT Career Start in 6 Months - Telerik Academy Alph')]")));

        Assert.assertTrue(firstResult.isDisplayed(),
                "IT Career Start in 6 Months - Telerik Academy Alpha is displayed");


        driver.quit();


    }


}

